﻿using EchoChat.Models;
using EchoChat.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using AutoMapper;

namespace EchoChat.Repository
{
    public class AppRepository : IAppRepository
    {
        IRepositoryBase _repositoryBase = null;
        private readonly IMapper _mapper;
        public AppRepository(IRepositoryBase repositoryBase, IMapper mapper)
        {
            _repositoryBase = repositoryBase;
            _mapper = mapper;
        }

        public string GetAppVersion(string platform)
        {
            string AppVersion = _repositoryBase.Get<AppVersion>(x => x.Platform == platform).Select(x => x.Version).FirstOrDefault();
            return AppVersion;
        }

        public bool SaveAppVersion(AppVersionDTO appRequest)
        {
            AppVersion appVersion = _mapper.Map<AppVersionDTO, AppVersion>(appRequest);
            _repositoryBase.Add<AppVersion>(appVersion);
            _repositoryBase.SaveChanges();
            return true;
        }
    }
}
