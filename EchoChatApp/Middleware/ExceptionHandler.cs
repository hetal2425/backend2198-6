﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;
using HttpContext = Microsoft.AspNetCore.Http.HttpContext;

namespace EchoChatApp.Middleware
{
    /// <summary>
    /// Class for Exception Handling
    /// </summary>
    public class ExceptionHandler
    {

        private readonly RequestDelegate _next;



        /// <summary>
        /// Logger Instance 
        /// </summary>
        private readonly ILogger<ExceptionHandler> _logger;



        /// <summary>
        /// Initializes the Exception Handler values.
        /// </summary>
        /// <param name="next"></param>
        /// <param name="loggerFactory"></param>
        public ExceptionHandler(RequestDelegate next, ILoggerFactory loggerFactory)
        {
            _next = next ?? throw new ArgumentNullException(nameof(next));
            _logger = loggerFactory?.CreateLogger<ExceptionHandler>() ?? throw new ArgumentNullException(nameof(loggerFactory));
        }

        /// <summary>
        /// Invokes the input request
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next.Invoke(context);

                // if request got failed from 400 Code on wards.
                if (context.Response.StatusCode > 399)
                {
                    await LogExceptionAsync(context);
                }
            }
            catch (Exception ex)
            {
                if (context.Response.HasStarted)
                {
                    _logger.LogWarning("The response has already started, the http status code middleware will not be executed.");
                    throw;
                }
                await LogExceptionAsync(context, ex);
            }
        }

        private async Task LogExceptionAsync(HttpContext context, Exception exception = null)
        {
            if (exception == null)
            {
                switch (context.Response.StatusCode)
                {
                    case (int)HttpStatusCode.BadRequest:
                        exception = new UnHandleException(context.Response.StatusCode, "Bad Request.");
                        exception.Source = "Services.SignalRPushMessageService";//context.Request.Path.Value + context.Request.QueryString.Value;
                        break;
                    case (int)HttpStatusCode.Unauthorized:
                        exception = new UnHandleException(context.Response.StatusCode, "UnAuthorized Request.");
                        exception.Source = "Services.SignalRPushMessageService";//context.Request.Path.Value + context.Request.QueryString.Value;
                        break;
                    case (int)HttpStatusCode.NotFound:
                        exception = new UnHandleException(context.Response.StatusCode, "Input Request not found.");
                        exception.Source = "Services.SignalRPushMessageService";//context.Request.Path.Value + context.Request.QueryString.Value;
                        break;
                    case (int)HttpStatusCode.InternalServerError:
                        exception = new UnHandleException(context.Response.StatusCode, "Internal Server Error.");
                        exception.Source = "Services.SignalRPushMessageService";//context.Request.Path.Value + context.Request.QueryString.Value;
                        break;
                    default:
                        exception = new Exception();
                        break;

                }
            }




            var result = JsonConvert.SerializeObject(new { error = exception.Message, errorId = context.Response.StatusCode });
            context.Response.ContentType = "application/json";
            await context.Response.WriteAsync(result);
        }
    }

    /// <summary>
    /// Customized Exception class to Un handle exceptions.
    /// </summary>
    public class UnHandleException : Exception
    {
        /// <summary>
        /// Status code.
        /// </summary>
        private readonly int StatusCode;

        /// <summary>
        /// Initializes the UnHandle exception values.
        /// </summary>
        /// <param name="statusCode"></param>
        /// <param name="message"></param>
        public UnHandleException(int statusCode, string message) : base(message)
        {
            StatusCode = statusCode;
        }

        /// <summary>
        /// Stack Trace
        /// </summary>
        public override string StackTrace => Environment.StackTrace;

        /// <summary>
        /// Method Base info.
        /// </summary>
        public new MethodBase TargetSite => MethodBase.GetCurrentMethod();
    }
}
