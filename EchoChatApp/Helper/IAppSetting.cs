﻿using EchoChat.Models;
using EchoChat.ServiceProvider;
using EchoChatApp.General;
using log4net;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace EchoChatApp.Helper
{
    public interface IAppSetting
    {
        public List<AppSettingsVM> FillAndGetAppSettings(int entityID);
    }
}