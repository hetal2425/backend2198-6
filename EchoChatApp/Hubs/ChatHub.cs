﻿using EchoChat.Models;
using EchoChat.ServiceProvider;
using EchoChatApp.General;
using log4net;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace EchoChatApp.Hubs
{
    public class ChatHub : Hub
    {
        private IUserProvider userProvider;
        private INoticeProvider noticeProvider;
        private ILog logger;

        public readonly IHubContext<ChatHub> _hubContext;
        private readonly static ConnectionMapping<string> _connections = new ConnectionMapping<string>();

        public ChatHub(IUserProvider oUserProvider, INoticeProvider oNoticeProvider, IHubContext<ChatHub> hubContext)
        {
            userProvider = oUserProvider;
            noticeProvider = oNoticeProvider;
            logger = Logger.GetLogger(typeof(ChatHub));
            _hubContext = hubContext;
        }

        public ChatHub()
        {
        }

        // Send Message from channel to plateform
        public async Task SendToCaller(IHubContext<ChatHub> context, HubNotice notice)
        {
            //foreach (var connectionId in _connections.GetConnections(notice.Createdby.ToString()))
            //{

            //    var outJson = "{\"NoticeId\": \"" + notice.ParentID + "\",\"GroupId\": \"" + notice.GroupID + "\",\"Message\": \"" + notice.NoticeDetail + "\"}";
            //    await context.Clients.Client(connectionId).SendAsync("ReceiveMessage", outJson);
            //}

            string jsonString = JsonConvert.SerializeObject(notice);
            await context.Clients.All.SendAsync("ReceiveMessage", jsonString);

            logger.Info("Message successfully sent from SignalR => " + jsonString);
        }

        // Display upload progress percentage count
        public async Task UserUploadProgress(IHubContext<ChatHub> context, string UserID, decimal percentage)
        {
            foreach (var connectionId in _connections.GetConnections(UserID))
            {
                await context.Clients.Client(connectionId).SendAsync("UserUploadProgress", percentage);
            }
        }

        // Create connection based on UserIds
        public override async Task OnConnectedAsync()
        {
            if (Context.GetHttpContext().Request.Query.Keys.Count > 1)
            {
                var UserId = Context.GetHttpContext().Request.Query["UserId"].ToString();
                _connections.Add(UserId, Context.ConnectionId);
            }

            //if (Context.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.PrimarySid)?.Value != null)
            //{
            //    string UserId = Guid.Parse(Context.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.PrimarySid)?.Value).ToString();
            //    _connections.Add(UserId, Context.ConnectionId);
            //}
            await base.OnConnectedAsync();
        }

        // Remove connection based on UserIds
        public override async Task OnDisconnectedAsync(Exception ex)
        {
            if (Context.GetHttpContext().Request.Query.Keys.Count > 1)
            {
                var UserId = Context.GetHttpContext().Request.Query["UserId"].ToString();
                _connections.Remove(UserId, Context.ConnectionId);
            }

            //if (Context.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.PrimarySid)?.Value != null)
            //{
            //    string UserId = Guid.Parse(Context.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.PrimarySid)?.Value).ToString();
            //    _connections.Remove(UserId, Context.ConnectionId);
            //}
            await base.OnDisconnectedAsync(ex);
        }


        public void SeenNotice(int NoticeID)
        {
            try
            {
                Guid userID = Guid.Parse(this.Context.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.PrimarySid)?.Value);
                noticeProvider.SeenNotice(NoticeID, userID, "Read");
            }
            catch (Exception ex)
            {
                logger.Error("SeenNotice: " + ex.Message);
            }
        }

        public void DeliverNotice(int NoticeID)
        {
            try
            {
                Guid userID = Guid.Parse(this.Context.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.PrimarySid)?.Value);
                noticeProvider.SeenNotice(NoticeID, userID, "Notify");
            }
            catch (Exception ex)
            {
                logger.Error("DeliverNotice: " + ex.Message);
            }
        }

        public void IsTyping(string groupID, string html)
        {
            SayWhoIsTyping(groupID, html);
        }

        public void SayWhoIsTyping(string groupID, string html)
        {
            Clients.Group(groupID.ToString()).SendAsync("SayWhoIsTyping", html);
        }
    }
}