﻿using EchoChat.Models;
using EchoChat.ServiceProvider;
using EchoChatApp.General;
using log4net;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EchoChatApp.Controllers
{
    [ApiController]
    [Authorize]
    [Route("api/[controller]")]
    public class TemplateController : ControllerBase
    {
        private ITemplateProvider templateProvider;
        private ILog logger;
        public TemplateController(ITemplateProvider oTemplateProvider)
        {
            templateProvider = oTemplateProvider;
            logger = Logger.GetLogger(typeof(ClientController));
        }

        [Route("AddTemplate")]
        [HttpPost]
        public IActionResult AddTemplate([FromForm] TemplateDTO template)
        {
            try
            {
                if (string.IsNullOrEmpty(template.Name))
                {
                    return BadRequest(new { Message = "Template Name is Required" });
                }
                else if (string.IsNullOrEmpty(template.Body))
                {
                    return BadRequest(new { Message = "Template Body is Required" });
                }
                else if (string.IsNullOrEmpty(template.Type))
                {
                    return BadRequest(new { Message = "Template Type is Required" });
                }

                bool isNameExist = templateProvider.CheckTemplateExistByName(template.Name);
                if (isNameExist)
                {
                    return BadRequest(new { Message = "Template Name Already Exist." });
                }

                templateProvider.AddTemplate(template);
                return Ok(true);
            }
            catch (Exception ex)
            {
                logger.Error("AddTemplate" + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [Route("UpdateTemplate")]
        [HttpPut]
        public IActionResult UpdateTemplate([FromForm] TemplateDTO template)
        {
            try
            {
                if (string.IsNullOrEmpty(template.Name))
                {
                    return BadRequest(new { Message = "Template Name is Required" });
                }
                else if (string.IsNullOrEmpty(template.Body))
                {
                    return BadRequest(new { Message = "Template Body is Required" });
                }
                else if (string.IsNullOrEmpty(template.Type))
                {
                    return BadRequest(new { Message = "Template Type is Required" });
                }

                bool isExist = templateProvider.CheckTemplateExist(template.Id);
                if (!isExist)
                {
                    return BadRequest(new { Message = "Template Not Exist." });
                }

                bool isNameExist = templateProvider.CheckTemplateExistByName(template.Name);
                if (isNameExist)
                {
                    return BadRequest(new { Message = "Template Name Already Exist." });
                }

                templateProvider.UpdateTemplate(template);
                return Ok(true);
            }
            catch (Exception ex)
            {
                logger.Error("UpdateTemplate" + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [Route("GetTemplate")]
        [HttpGet]
        public IActionResult GetTemplate(string title, string type, int pageSize, int pageNo)
        {
            try
            {

                var templateData = templateProvider.GetTemplate(title, type, pageSize, pageNo);
                if (templateData != null)
                {
                    return Ok(templateData);
                }
                return Ok(true);
            }
            catch (Exception ex)
            {
                logger.Error("GetTemplate" + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [Route("RemoveTemplate")]
        [HttpPost]
        public IActionResult RemoveTemplate(string Id)
        {
            try
            {
                if (string.IsNullOrEmpty(Id))
                {
                    return BadRequest(new { Message = "Template Id Required to Delete Specific Template." });
                }
                bool isExist = templateProvider.CheckTemplateExist(Convert.ToInt32(Id));

                if (!isExist)
                {
                    return BadRequest(new { Message = "Template Not Exist." });
                }

                templateProvider.RemoveTemplate(Convert.ToInt32(Id));
                return Ok(true);
            }
            catch (Exception ex)
            {
                logger.Error("RemoveTemplate" + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }
    }
}
