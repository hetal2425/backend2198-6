﻿using EchoChat.Channel;
using EchoChat.ChannelConnector;
using EchoChat.Constants;
using EchoChat.Helper;
using EchoChat.Models;
using EchoChat.ServiceProvider;
using EchoChatApp.General;
using EchoChatApp.Helper;
using log4net;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace EchoChatApp.Controllers
{
    [ApiController]
    [Authorize]
    [Route("api/[controller]")]
    public class ClientController : ControllerBase
    {
        private IClientProvider clientProvider;
        private IConnector connector;
        private ILog logger;
        public ClientController(IClientProvider oClientProvider, IConnector oConnector)
        {
            clientProvider = oClientProvider;
            connector = oConnector;
            logger = Logger.GetLogger(typeof(ClientController));
        }

        [Route("AddClient")]
        [HttpPost]
        [AllowAnonymous]
        public IActionResult AddClient([FromForm] ClientDTO client)
        {
            try
            {
                if (string.IsNullOrEmpty(client.OrganizationName))
                {
                    return BadRequest(new { Message = "Organization Name is Required" });
                }
                else if (string.IsNullOrEmpty(client.ContactPersonName))
                {
                    return BadRequest(new { Message = "Contact Person Name is Required" });
                }
                else if (string.IsNullOrEmpty(client.Email))
                {
                    return BadRequest(new { Message = "Email is Required" });
                }
                else if (string.IsNullOrEmpty(client.MobileNo))
                {
                    return BadRequest(new { Message = "MobileNo is Required" });
                }

                var isRegistered = clientProvider.CheckClientIsAlreadyRegistered(client.Email, client.MobileNo);

                if (isRegistered)
                {
                    return BadRequest(new { Message = "User is already registered" });
                }

                client.Password = Encryption.Encrypt(client.Password);
                if (client.ClientLogoFile != null)
                {
                    string BinaryFileName = DateTime.UtcNow.ToString("mmddyyyyhhmmssfff") + client.ClientLogoFile.FileName.Replace("\r", string.Empty);
                    client.ClientLogo = BinaryFileName;
                    var filestream = client.ClientLogoFile.OpenReadStream();
                    AWSHelper.AddClientLogo(filestream, null, BinaryFileName);
                }
                int clientID = clientProvider.SaveClient(client);

                if (clientID > 0)
                {
                    PrepareAndSendRegistrationMail(client.OrganizationName, client.Email, client.MobileNo, client.Password);
                    return Ok(true);
                }
                else
                {
                    return Ok(false);
                }
            }
            catch (Exception ex)
            {
                logger.Error("AddClient" + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        private void PrepareAndSendRegistrationMail(string organizationName, string email, string mobileNo, string password)
        {
            string Subject = "Welcome to EchoApp";

            string content = $"Hey <b>{organizationName}</b> <br/> You are Awesome and we are ecstatic to have you in the rapidly growing world of Echoers." +
                $"<br/><br/> Enjoy the journey with the Amazing features of EchoApp and increase your team's productivity and profitability." +
                $"<br/><br/> Here is a walkthrough video for you to understand all the functions around EchoApp. Feel free to drop me a line if you face any difficulty. " +
                $"For urgent help, use the Live Help option in the app." +
                $"<br/><br/>Amit Sengupta<br/>CEO<br/>EchoApp";

            MessageDetails md = new MessageDetails();
            md.Title = Subject;
            md.Message = content;
            md.SenderDetails = AWSHelper.sender;

            List<string> channels = new List<string>();
            channels.Add(ChannelMaster.EmailChannel);

            List<UserChannelData> userChannelDatas = new List<UserChannelData>();
            userChannelDatas.Add(new UserChannelData() { EmailID=email, UserID = string.Empty, MobileNo = mobileNo });

            connector.CallBroadcaster(md, channels, userChannelDatas, new List<AppSettingsVM>());

            string SubjectPW = "Login credentials for your recent signup";

            string contentPW = $"Dear <b>{organizationName} </b> Here are your sign in credentials for EchoApp " +
                $"<br/><br/> ID - <b>{mobileNo} </b><br/> Password - <b>{password} </b>" +
                $"<br/><br/> For all other users the default sign in details are as follows:" +
                $"<br/><br/> User ID - Mobile Number <br/> Password - ab@345" +
                $"<br/><br/> Every user other than you will be required to change the password at the time of their first login.Passwords can be changed at anytime within the app." +
                $"<br/><br/> We hope you enjoy using the app. For any issues you might face, please use the Live Help or drop us a line on support@echocommunicator.com." +
                $"<br/><br/> Best <br/> Amit Sengupta <br/> CEO <br/> EchoApp";


            md.Title = SubjectPW;
            md.Message = contentPW;

            connector.CallBroadcaster(md, channels, userChannelDatas, new List<AppSettingsVM>());
        }
    }
}