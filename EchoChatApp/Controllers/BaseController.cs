﻿using EchoChat.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Security.Claims;
using System.Xml.Linq;

namespace EchoChatApp.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class BaseController : ControllerBase
    {
        public Guid UserID { get { return Guid.Parse(this.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.PrimarySid)?.Value); } }
        public int ClientID { get { return int.Parse(this.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.PrimaryGroupSid)?.Value); } }
        public string UserName { get { return this.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Name)?.Value; } }
        public string ClientEmail { get { return this.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value; } }

        public string ClientName { get { return this.User.Claims.FirstOrDefault(c => c.Type == "ClientName")?.Value; } }
        public int TimeZoneOffset { get { int.TryParse(this.User.Claims.FirstOrDefault(c => c.Type == "TimeZoneOffset")?.Value, out int offset); return offset * -1; } }        
    }
}