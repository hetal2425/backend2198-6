﻿using EchoChat.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace EchoChat.ServiceProvider
{
    public interface ITemplateProvider
    {
        void AddTemplate(TemplateDTO template);
        void UpdateTemplate(TemplateDTO template);
        ListOfTemplates GetTemplate(string title, string type, int pageSize, int pageNo);
        void RemoveTemplate(int id);
        bool CheckTemplateExist(int id);
        bool CheckTemplateExistByName(string name);
    }
}
