﻿using EchoChat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Concurrent;

namespace EchoChat.ServiceProvider
{
    public interface INoticeProvider
    {
        bool SaveNoticeOutput(int noticeID, int clientID, ConcurrentBag<ChannelOutput> output);
        public void SeenNotice(int noticeID, Guid userID, string type);
        NoticeDetailDTO GetNoticeDetailsByNoticeID(int noticeID);
        NoticeSmallDataModel GetNoticeListByGroupID(int groupID, int parentID, int pageNo, int pageSize, Guid userID, string freeText, int timeZoneOffset);
        public List<DeletedNotice> DeleteNotice(int noticeID);
        public bool SaveNoticeDetails(NoticeDetailDTO noticeDetails);
        public bool SaveNotice(List<UserChannelInfo> userIdList, int noticeID, int groupID, Guid createdBy);
        List<ScheduledNotices> GetScheduledNotices(DateTime fromDate, DateTime toDate, int timeZoneOffset);
        public bool SaveIntegrationNoticeDetails(NoticeDetailDTO noticeDetails);
        public bool SaveIntegrationNotice(List<UserChannelInfo> userIdList, int noticeID, int groupID, Guid createdBy);
        public bool SavePrivateNoticeDetails(NoticeDetailDTO noticeDetails);
        public bool SavePrivateNotice(List<UserChannelInfo> userIdList, int noticeID, int groupID, Guid createdBy);
        public List<string> GetConfigWhatsAppTemplate();
        public List<string> GetConfigEmailTemplate();
        HubNotice GetNoticeDetailsForHub(int noticeID);
    }
}