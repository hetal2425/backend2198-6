﻿using EchoChat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace EchoChat.ServiceProvider
{
    public interface IClientProvider
    {
        public int SaveClient(ClientDTO client);
        public bool CheckClientIsAlreadyRegistered(string email, string mobileNo);
        List<AppSettingsVM> GetAppSettingsByEntityID(int entityID);
        string GetClientEmailById(int Id);
    }
}
