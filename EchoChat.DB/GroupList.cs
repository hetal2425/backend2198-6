﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EchoChat.DB
{
    public partial class GroupList
    {
        public int GroupListId { get; set; }
        public int GroupId { get; set; }
        public int ListId { get; set; }
    }
}
