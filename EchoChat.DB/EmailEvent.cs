﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EchoChat.DB
{
    public partial class EmailEvent
    {
        public int EmailEventId { get; set; }
        public string EventType { get; set; }
        public string MsgId { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string OpenByIpaddress { get; set; }
        public DateTime? Timestamp { get; set; }
    }
}
