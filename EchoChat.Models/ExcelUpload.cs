﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EchoChat.Models
{
    public class ExcelUpload
    {
        public int ListID { get; set; }
        public string ListName { get; set; }
        public IFormFile ExcelFile { get; set; }
    }
    public class ExcelUsers
    {
        public int ListID { get; set; }
        public string ListName { get; set; }
        public List<ExcelUserData> UserList { get; set; }

    }

    public class ExcelUserData
    {
        public string FirstName { get; set; }
        public string Email { get; set; }
        public string MobileNo { get; set; }
        public string IsAdmin { get; set; }
        public bool IsValid { get; set; }
        public string ErrorMessage { get; set; }
    }

    public class ExcelWhatsAppData
    {
        public string MobileNo { get; set; }
        public string TemplateName { get; set; }
        public string Value1 { get; set; }
        public string Value2 { get; set; }
        public string Value3 { get; set; }
        public string Value4 { get; set; }
        public string Value5 { get; set; }
        public string Value6 { get; set; }
        public string Value7 { get; set; }
        public string Value8 { get; set; }
        public string Value9 { get; set; }
        public string Value10 { get; set; }
        public string ErrorMessage { get; set; }
        public bool IsValid { get; set; }
    }
    public class WhatsAppUsersExcelUpload
    {
        public IFormFile ExcelFile { get; set; }
    }
}
