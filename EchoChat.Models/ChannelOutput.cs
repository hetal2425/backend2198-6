﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EchoChat.Models
{
    public class ChannelOutput
    {
        public string UserID { get; set; }
        public string ChannelName { get; set; }
        public string MsgID { get; set; }
        public string Error { get; set; }
        public bool IsSuccessful { get; set; }
        public string CustomUniqueID { get; set; }
    }
}
