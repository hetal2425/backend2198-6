﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EchoChat.Models
{
    public class AppSettingsVM
    {
        public int EntityID { get; set; }
        public string EntityName { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
        public string Details { get; set; }
        public string Value1 { get; set; }
        public string Value2 { get; set; }
        public string Value3 { get; set; }
        public string Value4 { get; set; }
        public string Value5 { get; set; }
        public string Value6 { get; set; }
        public string Value7 { get; set; }
    }
}
