﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EchoChat.Models
{
    public class ScheduledNotices
    {
        public string Subject { get; set; }
        public string Description { get; set; }
        public string Location { get; set; }
        public DateTime? StartTime { get; set; }
        public string StartTimezone { get; set; }
        public DateTime? EndTime { get; set; }
        public string EndTimezone { get; set; }
        public int IsAllDay { get; set; }
        public string RecurrenceRule { get; set; }
    }
}
