﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EchoChat.Models
{
    public class ChannelVerificationModel
    {
        public List<ChannelVerification> verifications { get; set; }
        public int Total { get; set; }
    }
    public class ChannelVerification
    {
        public Guid UserID { get; set; }
        public string UserName { get; set; }
        public string ProfilePhoto { get; set; }
        public bool IsOnSMS { get; set; }
        public bool IsOnEmail { get; set; }
        public bool IsOnWhatsApp { get; set; }
        public bool IsOnTelegram { get; set; }

    }
}
