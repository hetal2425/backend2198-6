﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EchoChat.Models
{
    public class Dashboard
    {
        public int TotalGroups { get; set; }
        public int TotalMembers { get; set; }
        public int TotalNotices { get; set; }
    }
}
