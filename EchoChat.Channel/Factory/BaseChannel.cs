﻿using System;
using System.Threading.Tasks;

namespace EchoChat.Channel.Factory
{
    public abstract class BaseChannel
    {
        Random random = new Random();
        public abstract Task Process();

        public void DelayTesting(string channel, string userID)
        {
            string msgID = Guid.NewGuid().ToString();
            Essentials.PushInOutput(channel, userID, msgID, string.Empty);            
            int randomBetween100And500 = random.Next(100, 500);
            Task.Delay(randomBetween100And500);
        }
    }
}
