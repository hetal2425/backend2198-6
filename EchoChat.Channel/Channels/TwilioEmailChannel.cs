﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Amazon;
using Amazon.SimpleEmail;
using Amazon.SimpleEmail.Model;
using EchoChat.Channel.Factory;
using EchoChat.Constants;
using EchoChat.Helper;
using EchoChat.Models;
using log4net;
using MimeKit;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace EchoChat.Channel
{
    public class TwilioEmailChannel : BaseChannel
    {
        private ILog logger = LogManager.GetLogger(typeof(TwilioEmailChannel));

        public override Task Process()
        {
            try
            {
                List<UserChannelData> userChannelDatas = Essentials.userChannelDatas.Where(u => !string.IsNullOrWhiteSpace(u.EmailID)).Select(u => u).Distinct().ToList();

                string Key = Essentials.settings.Where(a => a.Key.Equals(SettingMaster.SESConfigSet)).Select(a => a.Value).FirstOrDefault();

                if (userChannelDatas != null && userChannelDatas.Count > 0)
                {
                    string subject = Essentials.messageDetails.Title;

                    foreach (UserChannelData ucd in userChannelDatas)
                    {
                        if (!string.IsNullOrEmpty(ucd.EmailID))
                        {
                            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
                            Match match = regex.Match(ucd.EmailID);
                            if (match.Success)
                            {
                                var from = new EmailAddress(Essentials.messageDetails.SenderDetails);
                                var to = new EmailAddress(ucd.EmailID);
                                var plainTextContent = Essentials.messageDetails.Message;
                                var htmlContent = Essentials.messageDetails.HTMLMessage;

                                if (!string.IsNullOrEmpty(ucd.DisplayName))
                                {
                                    htmlContent = htmlContent.Replace(@"{UserName}", ucd.DisplayName);
                                }
                                else if (!string.IsNullOrEmpty(ucd.UserName))
                                {
                                    plainTextContent = plainTextContent.Replace(@"{UserName}", ucd.UserName);
                                }

                                var msg = MailHelper.CreateSingleEmail(from, to, subject, plainTextContent, htmlContent);

                                if (Essentials.messageDetails.Files != null && Essentials.messageDetails.Files.Count > 0)
                                {
                                    foreach (EchoFile echoFile in Essentials.messageDetails.Files)
                                    {
                                        var file = Convert.ToBase64String(GetFile(echoFile.FileName));
                                        msg.AddAttachment(echoFile.FileName, file);
                                    }
                                }

                                //DelayTesting(ChannelMaster.EmailChannel, ucd.UserID);
                                Send(ucd.UserID, msg, to, "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");
                            }
                        }
                    }
                }

                return Task.Delay(0);
            }
            catch (Exception ex)
            {
                logger.Error("Process: " + ex.Message);
                throw ex;
            }
        }

        private byte[] GetFile(string fileName)
        {
            try
            {
                var myfile = new WebClient().DownloadDataTaskAsync(new Uri(AWSHelper.GetPresignedFileURL(fileName)));
                myfile.Wait();
                return myfile.Result;
            }
            catch (Exception ex)
            {
                logger.Error("GetFile: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// sends email using AWS SES Api - using  SendRawEmail method.
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public void Send(string userID, SendGridMessage msg, EmailAddress to, string key)
        {
            try
            {
                using (var memoryStream = new MemoryStream())
                {
                    SendGridClient _emailService = new SendGridClient(key);
                    var response = _emailService.SendEmailAsync(msg);
                    response.Wait();

                    HttpHeaders headers = response.Result.Headers;
                    IEnumerable<string> values;
                    string EMSGID = string.Empty;
                    if (headers.TryGetValues("X-Message-Id", out values))
                    {
                        EMSGID = values.First();
                    }
                    if (response.Result.StatusCode == HttpStatusCode.Accepted)
                    {
                        Essentials.PushInOutput(ChannelMaster.EmailChannel, userID, EMSGID, string.Empty);
                        logger.Info($"The email with message Id {EMSGID} sent successfully to {to} on {DateTime.UtcNow:O}");
                    }
                    else
                    {
                        Essentials.PushInOutput(ChannelMaster.EmailChannel, userID, EMSGID, response.Result.IsSuccessStatusCode.ToString(), false);
                        logger.Info($"Failed to send email with message Id {EMSGID} to {to} on {DateTime.UtcNow:O} due to {response.Result.IsSuccessStatusCode}.");
                    }
                }
            }
            catch (Exception ex)
            {
                Essentials.PushInOutput(ChannelMaster.EmailChannel, userID, string.Empty, ex.Message, false);
                logger.Error($"Send: {ex}");
                throw ex;
            }
        }
    }
}
